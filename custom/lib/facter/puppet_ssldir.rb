require 'puppet'

Facter.add('puppet_ssldir') do
    setcode do
	Puppet.parse_config
	Puppet.settings.value('ssldir')
    end
end
