
Facter.add("augeas") do
    setcode do
        begin
            require "augeas.rb"
            true
        rescue LoadError
            false
        end
    end
end
