require 'puppet'

Facter.add('homename') do
    setcode do
        Puppet.parse_config
        Puppet.settings.value('certname')
    end
end
