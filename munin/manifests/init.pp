
# Install and configure munin node.
#
class munin::node {

    package { "munin-node":
        ensure => installed,
    }

    service { "munin-node":
        name    => $operatingsystem ? {
            OpenBSD => $operatingsystemrelease ? {
                /4\.[1-8]/ => "munin-node",
                default    => "munin_node",
            },
            default => "munin-node",
        },
        ensure  => running,
        enable  => true,
        require => Package["munin-node"],
        start   => $operatingsystem ? {
            OpenBSD => $operatingsystemrelease ? {
                /4\.[1-8]/ => "/usr/bin/install -d -o _munin /var/run/munin; /usr/local/sbin/munin-node",
                default    => undef,
            },
            default => undef,
        },
        stop    => $operatingsystem ? {
            OpenBSD => "/usr/bin/pkill -f /usr/local/sbin/munin-node",
            default => undef,
        },
    }

    file { "/etc/munin/munin-node.conf":
        ensure  => present,
        content => template("munin/munin-node.conf.erb"),
        owner   => "root",
        group   => $operatingsystem ? {
            OpenBSD => "wheel",
            default => "root",
        },
        mode    => "0644",
        require => Package["munin-node"],
        notify  => Exec["munin-node-configure"],
    }

    if $kernel == "Linux" {
        file { "/var/lib/munin/plugin-state":
            ensure  => directory,
            mode    => "0775",
            owner   => "munin",
            group   => "munin",
            before  => Service["munin-node"],
            require => Package["munin-node"],
        }
    }

    @@file { "/etc/munin/conf.d/${homename}.conf":
        ensure  => present,
        content => "[${homename}]\n  address ${ipaddress}\n  use_node_name yes\n",
        tag     => "munin",
    }

    exec { "munin-node-configure":
        command     => "munin-node-configure --shell --remove-also 2>/dev/null | /bin/sh",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
        user        => root,
        refreshonly => true,
        notify      => Service["munin-node"],
    }

}


# Configure SNMP node.
#
# === Parameters
#
#   $name:
#       Target SNMP host.
#   $snmp_community:
#       SNMP community. Defaults to public.
#   $snmp_version:
#       SNMP version. Defaults to 2.
#
# === Sample usage
#
# munin::snmpnode { "sw1.example.com":
#     snmp_community => "mycommunity",
# }
#
define munin::snmpnode($snmp_community="public", $snmp_version="2") {

    file { "/etc/munin/plugin-conf.d/snmp_${name}":
        ensure  => present,
        content => "[snmp_${name}_*]\nenv.community ${snmp_community}\nenv.version ${snmp_version}\n",
        owner   => root,
        group   => $operatingsystem ? {
            OpenBSD => wheel,
            default => root,
        },
        mode    => "0600",
        notify  => Service["munin-node"],
    }

    @@file { "/etc/munin/conf.d/${name}.conf":
        ensure  => present,
        content => "[${name}]\n  address ${ipaddress}\n  use_node_name no\n",
        tag     => "munin",
    }

}


# Add new custom munin plugin.
#
# === Parameters
#
#   $name:
#       Plugin name to install.
#   $config:
#       Configuration file name associated with plugin. Defaults to none.
#
# === Sample usage
#
# munin::plugin { "vmware_vms":
#     config => "vmware",
# }
#
define munin::plugin($config = "") {

    case $operatingsystem {
        OpenBSD: {
            file { "/usr/local/libexec/munin/plugins/${name}":
                ensure  => present,
                source  => "puppet:///modules/munin/plugins/${name}",
                owner   => "root",
                group   => "wheel",
                mode    => "0755",
                require => Package["munin-node"],
            }
        }
        default: {
            file { "/usr/share/munin/plugins/${name}":
                ensure  => present,
                source  => "puppet:///modules/munin/plugins/${name}",
                owner   => "root",
                group   => "root",
                mode    => "0755",
                require => Package["munin-node"],
            }
        }
    }

    if ($config) {
        file { "/etc/munin/plugin-conf.d/${config}":
            ensure  => present,
            source  => [ "puppet:///files/munin/plugin-conf/${config}.${fqdn}",
                         "puppet:///files/munin/plugin-conf/${config}",
                         "puppet:///modules/munin/plugin-conf/${config}", ],
            owner   => "root",
            group   => $operatingsystem ? {
                OpenBSD => wheel,
                default => root,
            },
            mode    => "0644",
            notify  => Service["munin-node"],
            require => $operatingsystem ? {
                OpenBSD => File["/usr/local/libexec/munin/plugins/${name}"],
                default => File["/usr/share/munin/plugins/${name}"],
           },
        }
    }

    case $operatingsystem {
        OpenBSD: {
            exec { "munin-enable-${name}":
                command => "ln -s /usr/local/libexec/munin/plugins/${name} /etc/munin/plugins/${name}",
                path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                user    => root,
                onlyif  => [ "test ! -h /etc/munin/plugins/${name}",
                             "/usr/local/libexec/munin/plugins/${name} autoconf", ],
                notify  => Service["munin-node"],
                require => File["/usr/local/libexec/munin/plugins/${name}"],
            }
        }
        default: {
            exec { "munin-enable-${name}":
                command => "ln -s /usr/share/munin/plugins/${name} /etc/munin/plugins/${name}",
                path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                user    => root,
                onlyif  => [ "test ! -h /etc/munin/plugins/${name}",
                             "/usr/share/munin/plugins/${name} autoconf", ],
                notify  => Service["munin-node"],
                require => File["/usr/share/munin/plugins/${name}"],
            }
        }
    }

}


# Install and configure munin server.
#
# === Requires
#
#  * Storedconfigs
#
class munin::server {

    package { "munin":
        ensure => installed,
    }

    if $munin_datadir {
        file { $munin_datadir:
            ensure  => directory,
            mode    => "0755",
            owner   => "munin",
            group   => "munin",
            seltype => "munin_var_lib_t",
            require => Package["munin"],
        }
        selinux::manage_fcontext { "${munin_datadir}(/.*)?":
            type   => "munin_var_lib_t",
            before => File[$munin_datadir],
        }

        file { "/var/lib/munin":
            ensure  => $munin_datadir,
            force   => true,
            backup  => ".orig",
            require => File[$munin_datadir],
        }
    }

    file { "/var/cache/munin":
        ensure  => directory,
        mode    => "0775",
        owner   => "munin",
        group   => $apache::sslserver::group,
        seltype => "httpd_munin_rw_content_t",
        require => Package["munin"],
    }
    selinux::manage_fcontext { "/var/cache/munin(/.*)?":
        type   => "httpd_munin_rw_content_t",
        before => File["/var/cache/munin"],
    }
    mount { "/var/cache/munin":
        ensure  => mounted,
        atboot  => true,
        device  => "none",
        fstype  => "tmpfs",
        options => "uid=munin,gid=${$apache::sslserver::group},mode=0775",
        dump    => "0",
        pass    => "0",
        require => File["/var/cache/munin"],
    }

    file { [ "/var/log/munin/munin-cgi-graph.log",
             "/var/log/munin/munin-cgi-html.log", ]:
        ensure  => present,
        mode    => "0664",
        owner   => "munin",
        group   => $apache::sslserver::group,
        require => Package["munin"],
    }

    file { "/var/www/html/munin/.htaccess":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [ "puppet:///files/munin/htaccess",
                     "puppet:///modules/munin/munin-htaccess", ],
        require => Package["munin"],
    }
    file { "/var/www/html/munin/cgi/.htaccess":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => "puppet:///modules/munin/cgi-htaccess",
        require => Package["munin"],
    }

    file { "/etc/munin/conf.d":
        ensure  => directory,
        purge   => true,
        force   => true,
        recurse => true,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        source  => "puppet:///modules/custom/empty",
        require => Package["munin"],
    }

    file { "/etc/munin/munin.conf":
        ensure  => present,
        owner   => "root",
        group   => "root",
        mode    => "0644",
        source  => "puppet:///modules/munin/munin.conf",
        require => Package["munin"],
    }

    File <<| tag == "munin" |>>

    define configwebhost() {
        file { "/srv/www/https/${name}/munin":
            ensure  => link,
            target  => "/var/www/html/munin",
            require => File["/srv/www/https/${name}"],
        }
    }

    if $munin_webhosts {
        apache::configfile { "munin.conf":
            http   => false,
            source => "puppet:///modules/munin/munin-httpd.conf",
        }

        configwebhost { $munin_webhosts: }
    }

}
