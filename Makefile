
MODULES = $(shell find */manifests/init.pp | sed -e 's/^\([^\/]*\).*/\1/')
MANIFESTS = $(shell find . -name \*.pp)

all: puppet-modules.tar.gz

puppet-modules.tar.gz: $(MODULES) LICENSE CREDITS Makefile.inc
	umask 022 ; tar zcvf $@ --owner=root --group=root \
		--mode g-w,o=g --exclude=.git --exclude=rdoc $^

check:
	@which puppet > /dev/null 2>&1 || ( \
		echo "puppet not installed, cannot perform syntax checking" \
		1>&2 ; exit 1 )
	@for name in `git diff --name-only`; do \
		echo $${name} | egrep '\.pp$$' > /dev/null || continue ; \
		errors="`puppet --color=false --parseonly --ignoreimport $${name}`" ; \
		[ $$? -eq 0 ] || echo "$${errors}" 1>&2 ; \
		errors="`egrep --line-number '[[:space:]]$$' $${name}`" ; \
		[ $$? -eq 0 ] && echo "$${name}: trailing white spaces on" \
			"line $${errors}" 1>&2 ; \
	done

check-all:
	@which puppet > /dev/null 2>&1 || ( \
		echo "puppet not installed, cannot perform syntax checking" \
		1>&2 ; exit 1 )
	@for name in `find . -name \*.pp` ; do \
		echo -n "$${name}: " ; \
		errors="`puppet --color=false --parseonly --ignoreimport $${name}`" ; \
		if [ $$? -eq 0 ]; then \
			echo "ok" ; \
		else \
			echo "error" ; \
			echo "$${errors}" 1>&2 ; \
		fi \
	done

lint:
	@rake lint

rdoc: $(MANIFESTS)
	mkdir .$$$$ ; \
		puppetdoc --mode rdoc --outputdir rdoc --modulepath . --manifestdir .$$$$ ; \
		rmdir .$$$$

clean:
	rm -rf puppet-modules.tar.gz rdoc
