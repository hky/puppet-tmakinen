
# Set sysctl value
#
# === Parameters
#
#   $name:
#       Sysctl key to set.
#   $value:
#       Value for given key.
#
# === Sample usage
#
# sysctl { "vm.swappinesss":
#     value => "100",
# }
#
define sysctl::set($value) {

    exec { "sysctl-${name}":
        command => "sysctl -w ${name}='${value}'",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless  => "test \"`sysctl -n ${name}`\" = \"${value}\"",
    }

    exec { "sysctl-${name}-save":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "echo '${name}=${value}' >> /etc/sysctl.conf",
        unless  => "egrep '^${name}=' /etc/sysctl.conf",
    }

}
