
# Install tftp server
#
# === Global variables:
#
#   $tftp_datadir:
#       Directory containing tftp files.
#
class tftp::server {

    if $tftp_datadir {
        file { $tftp_datadir:
            ensure  => directory,
            mode    => "0755",
            owner   => "root",
            group   => $operatingsystem ? {
                openbsd => "wheel",
                default => "root",
            },
            seltype => "tftpdir_t",
        }
        file { "/srv/tftpboot":
            ensure  => link,
            target  => $tftp_datadir,
            seltype => "tftpdir_t",
            require => File[$tftp_datadir],
        }
    } else {
        file { "/srv/tftpboot":
            ensure  => directory,
            mode    => "0755",
            owner   => "root",
            group   => $operatingsystem ? {
                "openbsd" => "wheel",
                default   => "root",
            },
            seltype => "tftpdir_t",
        }
    }

    case $operatingsystem {
        debian,fedora,ubuntu: {
            file { "/var/lib/tftpboot":
                ensure  => link,
                target  => "/srv/tftpboot",
                force   => true,
                require => File["/srv/tftpboot"],
            }
        }
        default: {
            file { "/tftpboot":
                ensure  => link,
                target  => "/srv/tftpboot",
                force   => true,
                require => File["/srv/tftpboot"],
            }
        }
    }

    if "${selinux}" == "true" {
        selinux::manage_fcontext { "/srv/tftpboot(/.*)?":
            type   => "tftpdir_t",
            before => File["/srv/tftpboot"],
        }
        if $tftp_datadir {
            selinux::manage_fcontext { "${tftp_datadir}(/.*)?":
                type   => "tftpdir_t",
                before => File[$tftp_datadir],
            }
        }
    }

    if $operatingsystem != "OpenBSD" {
        package { "tftp-server":
            name   => $operatingsystem ? {
                ubuntu  => "tftpd-hpa",
                debian  => "tftpd-hpa",
                default => "tftp-server",
            },
            ensure => installed,
        }
    }

    case $operatingsystem {
        debian,ubuntu: {
            service { "tftpd-hpa":
                ensure    => running,
                hasstatus => true,
                enable    => true,
                require   => [ File["/var/lib/tftpboot"],
                               Package["tftp-server"], ],
            }
        }
        default: {
            include inetd::server
            inetd::service { "tftp":
                ensure  => present,
                require => $operatingsystem ? {
                    "openbsd" => undef,
                    default   => Package["tftp-server"],
                },
            }
        }
    }

}


# Install tftp client tools
#
class tftp::client {

    package { "tftp":
        ensure => installed,
    }

}
