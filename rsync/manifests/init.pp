
# Install rsync
#
class rsync {

    package { "rsync":
        ensure => present,
    }

}

class rsync::run-rsync {
    case $operatingsystem {
        ubuntu: {
            package { "run-rsync": ensure => installed, }
            file { "/etc/run-rsync/RunRsyncConfig.pm":
                ensure  => present,
                source  => [ "puppet:///files/run-rsync/${homename}-RunRsyncConfig.pm",
                             "/etc/run-rsync/RunRsyncConfig.pm", ],
                mode    => "0644",
                owner   => "root",
                group   => "root",
            }
        }
    }
}
