
class sysstat {

    package { "sysstat":
        ensure => installed,
    }

    case $::operatingsystem {
        ubuntu: {
            file { "/etc/default/sysstat":
                ensure => present,
                source => "puppet:///modules/sysstat/sysstat.default",
                notify => Service["sysstat"],
            }
        }
        centos,fedora: {
        }
        default: {
            fail("sysstat not supported on ${::operatingsystem}")
        }
    }

    service { "sysstat":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package["sysstat"],
    }

}
