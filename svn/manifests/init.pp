# Install svn client tools.
#
class svn::client {

    package { "subversion":
        ensure => installed,
    }

    file { "/etc/subversion/servers":
        ensure  => present,
        mode    => "0644",
        owner   => root,
        group   => root,
        content => template("svn/servers.erb"),
        require => Package["subversion"],
    }

}


# Checkout working copy.
#
# === Parameters
#
#   $name:
#       Destination directory.
#   $source:
#       Source URL.
#   $ensure:
#       Revision. Defaults to HEAD.
#
# === Sample usage
#
# svn::checkout { "/usr/local/src/graphingwiki":
#     source => "http://svn.graphingwiki.webfactional.com/trunk",
#     ensure => "1959",
# }
#
define svn::checkout($source, $ensure="HEAD") {

    exec { "svn-co-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "svn checkout --non-interactive -r ${ensure} ${source} ${name}",
        creates => $name,
        require => [ Package["subversion"], File["/etc/subversion/servers"], ],
    }

    exec { "svn-up-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        cwd     => $name,
        command => "svn update --non-interactive -r ${ensure}",
        onlyif  => $ensure ? {
            "HEAD"  => "svn status --non-interactive -q -u 2>&1 | egrep '^[[:space:]]+\\*'",
            default => "test $(svn info --non-interactive 2>&1 | awk '/^Revision:/ { print \$2 }') != ${ensure}",
        },
        require => Exec["svn-co-${name}"],
    }

}


# Export directory from repository.
#
# === Parameters
#
#   $name:
#       Destination directory.
#   $source:
#       Source URL.
#   $ensure:
#       Revision. Defaults to HEAD.
#
# === Sample usage
#
# svn::export { "/usr/local/src/graphingwiki":
#     source => "http://svn.graphingwiki.webfactional.com/trunk/graphingwiki",
#     ensure => "1959",
# }
#
define svn::export($source, $ensure="HEAD") {

    exec { "svn-export-clean-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "rm -fr ${name}",
        onlyif  => $ensure ? {
            "HEAD"  => "test -r ${name}/.svnrevision && test $(svn info --non-interactive ${source} 2>&1 | awk '/^Last Changed Rev:/ { print \$4 }') != $(cat ${name}/.svnrevision)",
            default => "test -r ${name}/.svnrevision && test ${ensure} != $(cat ${name}/.svnrevision)",
        },
        require => [ Package["subversion"], File["/etc/subversion/servers"], ],
    }

    exec { "svn-export-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "svn export --non-interactive -r ${ensure} ${source} ${name} && svn info --non-interactive -r ${ensure} ${source} 2>&1 | awk '/^Last Changed Rev:/ { print \$4 }' > ${name}/.svnrevision",
        creates => $name,
        require => Exec["svn-export-clean-${name}"],
    }

}
