class amanda::common {

    file { "/var/lib/amanda/.ssh":
        ensure  => directory,
        mode    => "0700",
        owner   => "amandabackup",
        group   => "disk",
    }

    file { "/var/lib/amanda/.ssh/authorized_keys":
        ensure  => present,
        mode    => "0600",
        owner   => "amandabackup",
        group   => "disk",
        require => File["/var/lib/amanda/.ssh"],
    }

}


class amanda::client inherits amanda::common {

    if !$amanda_server {
        fail("\$amanda_server must be defined.")
    }

    if !$amanda_client {
        $amanda_client = $ipaddress
    }

    include inetd::server

    package { "amanda-enterprise-backup-client":
        ensure  => installed,
        before  => File["/var/lib/amanda/.ssh"],
        require => Class["inetd::server"],
    }

    file { [ "/etc/xinetd.d/amandaclient",
             "/etc/xinetd.d/zmrecover", ]:
        ensure  => absent,
        notify  => Service["xinetd"],
        require => Package["amanda-enterprise-backup-client"],
    }

    file { "/etc/amanda/amanda-client.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "amandabackup",
        group   => "disk",
        content => template("amanda/amanda-client.conf.erb"),
        require => Package["amanda-enterprise-backup-client"],
    }

    if $amanda_clientkey {
        @@ssh_authorized_key { "amrecover@${homename}":
            ensure  => present,
            key     => $amanda_clientkey,
            type    => "ssh-rsa",
            user    => "amandabackup",
            tag     => "amandaclient",
            options => [ "no-agent-forwarding", "no-port-forwarding", "no-X11-forwarding",
                         "command=\"/usr/lib/amanda/amandad -auth=ssh amindexd amidxtaped\"",
                         "from=\"${amanda_client}\"", ],
        }
    }

    Ssh_authorized_key <<| tag == "amandaserver" |>>

}


class amanda::server inherits amanda::common {

    if $amanda_serverkey {
        @@ssh_authorized_key { "amdump@${homename}":
            ensure  => present,
            key     => $amanda_serverkey,
            type    => "ssh-rsa",
            user    => "amandabackup",
            tag     => "amandaserver",
            options => [ "no-agent-forwarding", "no-port-forwarding", "no-X11-forwarding",
                         "command=\"/usr/lib/amanda/amandad -auth=ssh amdump\"",
                         "from=\"${amanda_server}\"", ],
        }
    }

    Ssh_authorized_key <<| tag == "amandaclient" |>>

}
