
# Enable S.M.A.R.T. monitoring daemon
#
class smart::daemon {

    package { "smartmontools":
        ensure => installed,
    }

    service { "smartd":
        ensure  => running,
        enable  => true,
        require => Package["smartmontools"],
    }

}
