# Install RPC portmapper service.
#
class portmap::server {

    case $::operatingsystem {
        "openbsd": {
            $package = ""
            $service = "portmap"
        }
        "centos": {
            case $::operatingsystemrelease {
                /^[1-5]\./: {
                    $package = "portmap"
                    $service = "portmap"
                }
                default: {
                    $package = "rpcbind"
                    $service = "rpcbind"
                }
            }
        }
        "fedora": {
            $package = "rpcbind"
            $service = "rpcbind"
        }
        "ubuntu": {
            $package = "rpcbind"
            $service = "portmap"
        }
        default: {
            fail("portmap::server not supported on ${::operatingsystem}")
        }
    }

    if $package {
        package { "portmap":
            ensure => installed,
            name   => $package,
            before => Service["portmap"],
        }
    }

    service { "portmap":
        ensure => running,
        name   => $service,
        enable => true,
    }

}
