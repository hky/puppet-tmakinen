# Install DNS server.
#
class dns::server {

    if $operatingsystem != "OpenBSD" {
        package { "bind":
            name => $operatingsystem ? {
                "ubuntu" => "bind9",
                "centos" => $operatingsystemrelease ? {
                    /^5\..*/ => [ "bind-chroot", "caching-nameserver", ],
                    default  => "bind-chroot",
                },
                default  => "bind-chroot",
            }
        }
    }

    # first set per os paths
    case $operatingsystem {
        "fedora": {
            $confdir = "/etc/named"
        }
        "centos": {
            case $operatingsystemrelease {
                /^5\..*/: {
                    $chroot = "/var/named/chroot"
                }
                default: {
                    $confdir = "/etc/named"
                }
            }
        }
        "ubuntu": {
            $confdir = "/etc/bind"
            $config = "${confdir}/named.conf"
            $rndckey = "${confdir}/rndc.key"
        }
        "openbsd": {
            $chroot = "/var/named"
        }
    }
    # if some var is not set use default value
    if !$confdir {
        $confdir = "/etc"
    }
    if !$config {
        $config = "/etc/named.conf"
    }
    if !$rndckey {
        $rndckey = "/etc/rndc.key"
    }
    if !$chroot {
        $chroot = ""
    }

    case $operatingsystem {
        "ubuntu": {
            $group = "bind"
        }
        default: {
            $group = "named"
        }
    }

    file { "${chroot}${rndckey}":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => $group,
        require => $operatingsystem ? {
            "openbsd" => undef,
            default   => Package["bind"],
        },
    }
    exec { "rndc-confgen":
        command => $chroot ? {
            ""      => "rndc-confgen -r /dev/urandom -a",
            default => "rndc-confgen -r /dev/urandom -a -t ${chroot}",
        },
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless  => "test -s ${chroot}${rndckey}",
        require => File["${chroot}${rndckey}"],
    }
    if "${chroot}" != "" {
        file { "/etc/rndc.key":
            ensure  => link,
            target  => "${chroot}${rndckey}",
            owner   => "root",
            group   => $group,
            require => Exec["rndc-confgen"],
        }
    }

    service { "named":
        name    => $operatingsystem ? {
            "ubuntu" => "bind9",
            default  => "named",
        },
        ensure  => running,
        enable  => true,
        status  => "/usr/sbin/rndc status",
        stop    => $operatingsystem ? {
            "openbsd" => "pkill -u named",
            default   => undef,
        },
        start   => $operatingsystem ? {
            "openbsd" => "/usr/sbin/named",
            default   => undef,
        },
        require => Exec["rndc-confgen"],
    }

    file { "named.conf":
        ensure  => present,
        path    => "${chroot}${config}",
        mode    => "0640",
        owner   => "root",
        group   => $group,
        require => $operatingsystem ? {
            "openbsd" => undef,
            default   => Package["bind"],
        },
        notify  => Exec["generate-named-conf"],
    }
    file { "/usr/local/sbin/generate-named-conf.sh":
        ensure  => present,
        content => template("dns/generate-named-conf.sh.erb"),
        mode    => "0755",
        owner   => "root",
        group   => $operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        notify  => Exec["generate-named-conf"],
    }
    exec { "generate-named-conf":
        command     => "/usr/local/sbin/generate-named-conf.sh > ${chroot}${config}",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        user        => "root",
        refreshonly => true,
        require     => File["/usr/local/sbin/generate-named-conf.sh"],
        notify      => Service["named"],
    }

    file { "${chroot}${confdir}/named.conf.options":
        ensure  => present,
        source  => [ "puppet:///files/dns/named.conf.options.${fqdn}",
                     "puppet:///files/dns/named.conf.options",
                     "puppet:///modules/dns/named.conf.options", ],
        mode    => "0640",
        owner   => "root",
        group   => $group,
        require => $operatingsystem ? {
            "openbsd" => undef,
            default   => Package["bind"],
        },
        notify  => Service["named"],
    }

    file { "${chroot}${confdir}/named.conf.local":
        ensure  => present,
        source  => [ "puppet:///files/dns/named.conf.local.${fqdn}",
                     "puppet:///files/dns/named.conf.local",
                     "puppet:///modules/dns/named.conf.local", ],
        mode    => "0640",
        owner   => "root",
        group   => $group,
        require => $operatingsystem ? {
            "openbsd" => undef,
            default   => Package["bind"],
        },
        notify  => Service["named"],
    }
}

# Generate named config from LDAP
#
# Usage:
#
# Put templates you want to generate to master.in directory.
# See dnsdump.py for example template tags.

class dns::server::ldap inherits dns::server {

    include ldap::client::python

    file { "/usr/local/sbin/dnsdump.py":
        ensure  => present,
        source  => "puppet:///modules/dns/dnsdump.py",
        mode    => 0755,
        owner   => root,
        group   => $operatingsystem ? {
            OpenBSD => wheel,
            default => root,
        },
    }

    file { "/var/named/master.in":
        ensure  => directory,
        source  => "puppet:///files/dns/master.in",
        recurse => true,
        mode    => 0755,
        owner   => root,
        purge   => true,
    }

    exec { "generate-dns-conf":
        path      => "/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
        command   => "dnsdump.py --notest /var/named/master.in /var/named/master",
        require   => File["/usr/local/sbin/dnsdump.py"],
        unless    => "dnsdump.py --test /var/named/master.in /var/named/master",
        notify    => Service["named"]
    }
}

# Configure DNS zone.
#
# === Parameters
#
# $name:
#      Zone name.
# $role:
#       The role {master, slave, dynamic, forward} of this host.
# $master:
#      IP address of DNS master for this zone if role is slave.
#      IP address and FQDN of DNS master for this zone if running as
#      master and using autogenerated zone.
# $slaves:
#      IP addresess and host names of the DNS slaves for this zone.
#      Required only when using autogenrated zones.
# $forwarders:
#      Array of forwarder IP addresses for forward role zones.
# $source:
#      Source file to use for zone. Defaults to auto.
# $key:
#      Key for dynamic zones.
# $keytype:
#      Key algorithm. Defaults to 'hmac-md5'.
#
define dns::zone($role = "master", $master = "", $slaves = [], $forwarders = [],
                 $source = "AUTO", $key = "none", $keytype = "hmac-md5") {

    $tmpname = regsubst($name, '([^/]+/)?([0-9]+)/([0-9\.]+\.in-addr\.arpa)', '\1\2-\3')
    case dirname($tmpname) {
        ".":       { $view = "" }
        "default": { $view = "" }
        default:   { $view = regsubst(dirname($tmpname), '^(.*)', '\1/') }
    }
    $zone = basename($tmpname)

    case $role {
        "master": {
            case $operatingsystem {
                "openbsd": {
                    $zonedir = "/master/${view}"
                }
                "fedora","centos": {
                    $zonedir = "/var/named/${view}"
                }
                "ubuntu": {
                    $zonedir = "/etc/bind/${view}"
                }
            }
        }
        "dynamic": {
            if $key == "none" {
                fail("No key defined for dns::zone '${name}'")
            }
            case $operatingsystem {
                "openbsd": {
                    $zonedir = "/dynamic/${view}"
                }
                "fedora","centos": {
                    $zonedir = "/var/named/dynamic/${view}"
                }
                "ubuntu": {
                    $zonedir = "/etc/bind/dynamic/${view}"
                }
            }
        }
        "slave": {
            if $master == "" {
                fail("No master defined for dns::zone '${name}'")
            }
            case $operatingsystem {
                "openbsd": {
                    $zonedir = "/slave/${view}"
                }
                "fedora","centos": {
                    $zonedir = "/var/named/slaves/${view}"
                }
                "ubuntu": {
                    $zonedir = "/var/cache/bind/${view}"
                }
            }
        }
        "forward": {
            if $forwarders == [] {
                fail("No forwarders defined for dns::zone '${name}'")
            }
            $zonedir = ""
        }
        default: {
            fail("Unknown DNS zone type '${role}'")
        }
    }

    if $view != "" {
        if !defined(File["${dns::server::chroot}${dns::server::confdir}/${view}"]) {
            file { "${dns::server::chroot}${dns::server::confdir}/${view}":
                ensure => directory,
                mode   => "0750",
                owner  => "root",
                group  => $dns::server::group,
                before => File["${dns::server::chroot}${dns::server::confdir}/${view}zone.${zone}"],
            }
        }
    }

    if $zonedir != "" {
        if !defined(File["${dns::server::chroot}${zonedir}"]) {
            file { "${dns::server::chroot}${zonedir}":
                ensure => directory,
                mode   => $role ? {
                    "master" => "0750",
                    default  => "0770",
                },
                owner  => "root",
                group  => $dns::server::group,
                before => $role ? {
                    "master" => File["${dns::server::chroot}${zonedir}db.${zone}"],
                    default  => undef,
                },
            }
        }
    }

    file { "${dns::server::chroot}${dns::server::confdir}/${view}zone.${zone}":
        ensure  => present,
        content => template("dns/zone.$role.erb"),
        mode    => "0640",
        owner   => "root",
        group   => $dns::server::group,
        require => $operatingsystem ? {
            "openbsd" => undef,
            default   => Package["bind"],
        },
        notify  => Exec["generate-named-conf"],
    }

    if $role == "master" and $zone != "." {
        if $source != "AUTO" {
             file { "${dns::server::chroot}${zonedir}db.${zone}":
                 ensure  => present,
                 source  => $source,
                 mode    => "0640",
                 owner   => "root",
                 group   => $dns::server::group,
                 require => $operatingsystem ? {
                     "openbsd" => undef,
                     default   => Package["bind"],
                 },
                 notify  => Exec["generate-named-conf"],
            }
        } else {
            file { "${dns::server::chroot}${zonedir}/db.${zone}":
                ensure  => present,
                content => template("dns/db.erb"),
                mode    => "0640",
                owner   => "root",
                group   => $dns::server::group,
                require => $operatingsystem ? {
                    "openbsd" => undef,
                    default   => Package["bind"],
                },
                notify  => Service["named"],
            }
            file { "${dns::server::chroot}${zonedir}/db.${zone}-dynamic":
                ensure  => present,
                source  => [
                  "puppet:///files/dns/db.${zone}-dynamic.${homename}",
                  "puppet:///modules/dns/empty",
                ],
                mode    => "0640",
                owner   => "root",
                group   => $dns::server::group,
                require => $operatingsystem ? {
                    "openbsd" => undef,
                    default   => Package["bind"],
                },
                notify  => Service["named"],
            }
            file { "${dns::server::chroot}${zonedir}/db.${zone}-static":
                ensure  => present,
                source  => [
                  "puppet:///files/dns/db.${zone}-static.${homename}",
                  "puppet:///modules/dns/empty",
                ],
                mode    => "0640",
                owner   => "root",
                group   => $dns::server::group,
                require => $operatingsystem ? {
                    "openbsd" => undef,
                    default   => Package["bind"],
                },
                notify  => Service["named"],
            }
        }
    }

}


# Install dynamic DNS update script
#
# === Global variables
#
#   $dns_nsupdate_name:
#       FQDN to update into DNS.
#
#   $dns_nsupdate_key:
#       DNS key to use when updating entry. Usually in format:
#         <keyname> <secret>
#       for example:
#         gw1.example.com. sZ6GgTZLBX83LXCoo
#
#   $dns_nsupdate_server:
#       DNS server address where to update entry.
#
#   $dns_nsupdate_zone:
#       Zone name to update. Defaults to domain part of
#       $dns_nsupdate_name variable.
#
class dns::nsupdate {

    file { "/usr/local/sbin/nsupdate.sh":
        ensure  => present,
        content => template("dns/nsupdate.sh.erb"),
        mode    => "0700",
        owner   => root,
        group   => $operatingsystem ? {
            openbsd => wheel,
            default => root,
        },
    }

    cron { "nsupdate":
        ensure  => present,
        command => "/usr/local/sbin/nsupdate.sh",
        minute  => "*/5",
        require => File["/usr/local/sbin/nsupdate.sh"],
    }

}
