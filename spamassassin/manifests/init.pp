
# Install spamassassin common components
#
class spamassassin::common {

    package { "spamassassin":
        ensure => present,
    }

    service { "spamassassin":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package["spamassassin"],
    }

}

# Install spamassassin using procmail
#
class spamassassin::procmail inherits spamassassin::common {

    include procmail

    procmail::rc { "spamassassin.rc":
        source  => "/etc/mail/spamassassin/spamassassin-spamc.rc",
        require => Package["spamassassin"],
    }

}
