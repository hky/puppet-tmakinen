# Install rubygems.
#
class ruby::rubygems {

    tag("bootstrap")

    require gcc
    require gnu::make

    package { "ruby-devel":
        ensure => installed,
        name   => $::operatingsystem ? {
            "ubuntu" => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'ruby\1-dev'),
            default  => "ruby-devel",
        },
    }

    package { "rubygems":
        ensure  => installed,
        require => Package["ruby-devel"],
    }

}


# Install mysql ruby bindings.
#
class ruby::mysql {

    package { "ruby-mysql":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libmysql-ruby\1'),
            "ubuntu" => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libmysql-ruby\1'),
            default  => "ruby-mysql",
        },
    }

}


# Install rails.
#
class ruby::rails {

    case $::operatingsystem {
        "centos": {
            if $::operatingsystemrelease =~ /^[1-5]/ {
                package { "rubygem-rails":
                    ensure => installed,
                }
            } else {
                require ruby::rubygems
                package { "rubygem-rails":
                    ensure   => "2.3.14",
                    name     => "rails",
                    provider => "gem",
                }
            }
        }
        default: {
            package { "rubygem-rails":
                ensure => installed,
                name   => $::operatingsystem ? {
                    "debian"  => "rails",
                    "ubuntu"  => "rails",
                    "openbsd" => "ruby-rails",
                    default   => "rubygem-rails",
                },
            }
        }
    }

}


# Install rrd ruby bindings.
#
class ruby::rrd {

    package { "ruby-rrd":
        ensure => installed,
        name   => $::operatingsystem ? {
            centos  => $::operatingsystemrelease ? {
                /^[1-5]/ => "ruby-RRDtool",
                default  => "rrdtool-ruby",
            },
            debian  => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'librrd-ruby\1'),
            ubuntu  => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'librrd-ruby\1'),
            openbsd => "ruby-rrd",
            default => "ruby-RRDtool",
        },
    }

}


# Install sqlite3 ruby bindings.
#
class ruby::sqlite3 {

    case $::operatingsystem {
        "centos": {
            if $::operatingsystemrelease =~ /^[1-5]/ {
                package { "rubygem-sqlite3-ruby":
                    ensure => installed,
                }
            } else {
                require ruby::rubygems
                package { [ "sqlite", "sqlite-devel", ]:
                    ensure => installed,
                    before => Package["rubygem-sqlite3"],
                }
                package { "rubygem-sqlite3":
                    ensure   => installed,
                    name     => "sqlite3",
                    provider => "gem",
                }
            }
        }
        default: {
            package { "rubygem-sqlite3-ruby":
                ensure => installed,
                name   => $::operatingsystem ? {
                    "debian"  => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libsqlite3-ruby\1'),
                    "ubuntu"  => regsubst($::rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libsqlite3-ruby\1'),
                    "openbsd" => "ruby-sqlite3",
                    default   => "rubygem-sqlite3-ruby",
                },
            }
        }
    }

}
