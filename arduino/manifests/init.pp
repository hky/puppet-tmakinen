
# Install Arduino development tools
#
class arduino {

    package { [ "uisp",
                "avr-libc",
                "avr-gcc-c++",
                "rxtx",
                "avrdude", ]:
        ensure => installed,
        before => File["/usr/local/bin/arduino"],
    }

    if !$arduino_package {
        case $architecture {
            "x86_64": {
                if $arduino64_package_latest {
                    $arduino_package = $arduino64_package_latest
                } else {
                    fail("Must define \$arduino_package or \$arduino64_package_latest")
                }
            }
            default: {
                if $arduino32_package_latest {
                    $arduino_package = $arduino32_package_latest
                } else {
                    fail("Must define \$arduino_package or \$arduino32_package_latest")
                }
            }
        }
    }

    file { "/usr/local/src/arduino.tgz":
        ensure => present,
        source => "puppet:///files/packages/${arduino_package}",
        mode   => "0644",
        owner  => "root",
        group  => "root",
    }
    util::extract::tar { "/usr/local/lib/arduino":
        strip   => 1,
        source  => "/usr/local/src/arduino.tgz",
        require => File["/usr/local/src/arduino.tgz"],
    }

    file { "/usr/local/bin/arduino":
        ensure  => link,
        target  => "/usr/local/lib/arduino/arduino",
        owner   => "root",
        group   => "root",
        require => Util::Extract::Tar["/usr/local/lib/arduino"],
    }

}
