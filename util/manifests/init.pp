# Extract tar package.
#
# === Parameters
#
#   $name:
#       Destination directory.
#   $ensure:
#       If 'latest', remove target and re-extract when source file changes.
#   $source:
#       File to extract.
#   $strip:
#       Remove the specified number of leading path elements.
#       Defaults to 0.
#   $preserve:
#       Preserve owner and permissions. Defaults to false.
#
# === Sample usage
#
#util::extract::tar { "/usr/local/src/moin-1.8.8":
#    strip  => 1,
#    source => "/usr/local/src/moin-1.8.8.tar.gz",
#}
#
define util::extract::tar($source, $ensure=present, $strip=0, $preserve=false) {

    include gnu::tar

    case $ensure {
        latest: {
            exec { "tar-rmdir-${name}":
                path        => "/bin:/usr/bin:/sbin:/usr/sbin",
                command     => "rm -fr ${name}",
                before      => File[$name],
                subscribe   => File[$source],
                refreshonly => true,
            }
        }
    }

    file { $name:
        ensure => directory,
        mode   => "0755",
        owner  => "root",
        group  => $operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

    case regsubst($source, '.*\.([^.]+)$', '\1') {
        tar: { $cat = "cat" }
        gz,tgz: { $cat = "zcat" }
        bz2, tbz: { $cat = "bzcat" }
    }

    $tar = $operatingsystem ? {
        "openbsd" => "gtar",
        default   => "tar",
    }

    if $preserve {
        $command = "/bin/sh -c 'umask 022; ${cat} ${source} | ${tar} xf - --strip-components=${strip} -C ${name}'"
    } else {
        $command = "/bin/sh -c 'umask 022; ${cat} ${source} | ${tar} xf - --strip-components=${strip} -C ${name} --no-same-owner --no-same-permissions'"
    }

    exec { "tar-extract-${name}":
        path    => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
        command => $command,
        require => [ File[$name], Package["tar"], ],
        unless  => "test -n \"$(ls -A ${name})\"",
    }

}

# Extract tar package.
#
# === Parameters
#
#   $name:
#       Destination directory.
#   $ensure:
#       If 'latest', remove target and re-extract when source file changes.
#   $source:
#       File to extract.
#
# === Sample usage
#
#util::extract::zip { "/usr/lib/firefox-addons/extensions/{73a6fe31-595d-460b-a920-fcc0f8843232}":
#    source => "/usr/local/src/noscript.xpi",
#}
#
define util::extract::zip($source, $ensure=present) {

    case $ensure {
        latest: {
            exec { "zip-rmdir-${name}":
                path        => "/bin:/usr/bin:/sbin:/usr/sbin",
                command     => "rm -fr ${name}",
                before      => File[$name],
                subscribe   => File[$source],
                refreshonly => true,
            }
        }
    }

    $command = "unzip ${source} -d ${name}"

    file { $name:
        ensure => directory,
        mode   => "0755",
        owner  => root,
        group  => root,
    }

    exec { "zip-extract-${name}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => $command,
        require => File[$name],
        unless  => "test -n \"$(ls -A ${name})\"",
    }

}

# Install patch.
#
class util::patch::package {

    if $kernel == "Linux" {
        package { "patch":
            ensure => installed,
        }
    }

}


# Apply patch.
#
# === Parameters
#
#   $name:
#       Destination directory.
#   $source:
#       Patch file.
#   $strip:
#       Remove the specified number of leading path elements.
#       Defaults to 0.
#
# === Sample usage
#
# util::patch { "/usr/local/src/moin-1.8.8":
#     strip  => 1,
#     source => "/usr/local/src/moin.patch",
# }
#
define util::patch($source, $strip=0) {

    include util::patch::package

    exec { "patch-${name}-${source}":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        cwd     => $name,
        command => "patch -N -b -t -p${strip} < ${source}",
        onlyif  => "patch --dry-run -N -b -t -p${strip} < ${source}",
        require => Class["util::patch::package"],
    }

}
