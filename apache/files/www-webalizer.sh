#!/bin/sh

umask 022

for cfg in /etc/webalizer/http/*.conf /etc/webalizer/https/*.conf; do
    test -f ${cfg} && /usr/bin/webalizer -c ${cfg}
done
