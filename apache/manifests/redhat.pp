class apache::redhat::server {

    file { [ "/etc/httpd/conf.http.d",
             "/etc/httpd/site.http.d",
             "/srv/www/http",
             "/srv/www/http/${homename}",
             "/srv/www/log/http",
             "/srv/www/log/http/${homename}", ]:
        ensure  => directory,
        mode    => "0644",
        owner   => root,
        group   => root,
        require => Package["httpd"],
        before  => File["/etc/httpd/conf/httpd.conf"],
    }

    File["/etc/httpd/conf.http.d", "/etc/httpd/site.http.d"] {
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
    }

    File["/srv/www/log/http", "/srv/www/log/http/${homename}"] {
        seltype => "httpd_log_t",
    }

    file { "/etc/httpd/conf/httpd.conf":
        ensure  => present,
        content => template("apache/httpd.conf.erb"),
        mode    => "0644",
        owner   => root,
        group   => root,
        require => Package["httpd"],
        notify  => Service["httpd"],
    }

    service { "httpd":
        ensure  => running,
        enable  => true,
        require => [ Package["httpd"],
                     File["/etc/httpd/conf/httpd.conf"], ],
    }

}


define apache::redhat::site($aliases, $root, $redirect) {

    if $name == "default" {
        $site_fqdn = $homename
        $site_conf = "/etc/httpd/site.http.d/00-${site_fqdn}.conf"
        $site_confdir = "/etc/httpd/site.http.d/00-${site_fqdn}.d"
    } else {
        $site_fqdn = $name
        $site_conf = "/etc/httpd/site.http.d/${site_fqdn}.conf"
        $site_confdir = "/etc/httpd/site.http.d/${site_fqdn}.d"

        if !$redirect {
            if $root {
                file { "/srv/www/http/${site_fqdn}":
                    ensure => link,
                    target => $root,
                    before => File[$site_conf],
                }
            } else {
                file { "/srv/www/http/${site_fqdn}":
                    ensure => directory,
                    mode   => "0755",
                    owner  => root,
                    group  => root,
                    before => File[$site_conf],
                }
            }

            file { "/srv/www/log/http/${site_fqdn}":
                ensure => directory,
                mode   => "0755",
                owner  => root,
                group  => root,
                before => File[$site_conf],
            }
        }
    }

    file { $site_conf:
        ensure => present,
        mode   => "0644",
        owner  => root,
        group  => root,
        notify => Service["httpd"],
    }

    if $redirect {
        File[$site_conf] {
            content => "<VirtualHost *:80>\n  ServerName ${site_fqdn}\n  Redirect permanent / ${redirect}\n</VirtualHost>\n",
        }
    } else {
        File[$site_conf] {
            content => template("apache/site.http.conf.erb"),
        }
        file { $site_confdir:
            ensure  => directory,
            mode    => "0644",
            owner   => root,
            group   => root,
            purge   => true,
            force   => true,
            recurse => true,
            source  => [ "puppet:///files/apache/sites/${site_fqdn}",
                         "puppet:///modules/custom/empty", ],
            before  => File[$site_conf],
            notify  => Service["httpd"],
        }
    }

}


class apache::redhat::sslserver {

    package { "mod_ssl":
        ensure => installed
    }

    file { [ "/etc/httpd/conf.https.d",
             "/etc/httpd/site.https.d",
             "/srv/www/https",
             "/srv/www/https/${homename}",
             "/srv/www/log/https",
             "/srv/www/log/https/${homename}", ]:
        ensure  => directory,
        mode    => "0644",
        owner   => root,
        group   => root,
        require => Package["httpd"],
        before  => File["/etc/httpd/conf/httpsd.conf"],
    }

    File["/etc/httpd/conf.https.d", "/etc/httpd/site.https.d"] {
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
    }

    File["/srv/www/log/https", "/srv/www/log/https/${homename}"] {
        seltype => "httpd_log_t",
    }

    file { "/etc/httpd/conf/httpsd.conf":
        ensure  => present,
        mode    => "0644",
        owner   => root,
        group   => root,
        content => template("apache/httpsd.conf.erb"),
        require => Package["httpd"],
        notify  => Service["httpsd"],
    }

    case $operatingsystem {
        "fedora": {
            file { "/lib/systemd/system/httpsd.service":
                ensure => present,
                source => "puppet:///modules/apache/httpsd.service",
                mode   => "0644",
                owner  => "root",
                group  => "root",
                before => Service["httpsd"],
            }
        }
        default: {
            file { "/etc/init.d/httpsd":
                ensure => present,
                source => "puppet:///modules/apache/httpsd",
                mode   => "0755",
                owner  => "root",
                group  => "root",
                before => Service["httpsd"],
            }
        }
    }

    file { "/etc/sysconfig/httpsd":
        ensure  => present,
        content => "",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        notify  => Service["httpsd"],
    }

    file { "/usr/sbin/httpsd":
        ensure  => link,
        target  => "/usr/sbin/httpd",
        seluser => "system_u",
        selrole => "object_r",
        seltype => "httpd_exec_t",
        require => Package["httpd"],
    }

    service { "httpsd":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => [ Package["httpd"],
                       Package["mod_ssl"],
                       File["/etc/httpd/conf/httpsd.conf"],
                       File["/usr/sbin/httpsd"], ],
    }

}


define apache::redhat::sslsite($ipaddr, $root, $ssl_cert, $ssl_key, $ssl_chain) {

    if $name == "default" {
        $site_fqdn = $homename
    } else {
        $site_fqdn = $name

        if $root {
            file { "/srv/www/https/${site_fqdn}":
                ensure => link,
                target => $root,
                before => Service["httpsd"],
            }
        } else {
            file { "/srv/www/https/${site_fqdn}":
                ensure => directory,
                mode   => "0755",
                owner  => root,
                group  => root,
                before => Service["httpsd"],
            }
        }

        file { "/srv/www/log/https/${site_fqdn}":
            ensure  => directory,
            mode    => "0755",
            owner   => "root",
            group   => "root",
            seltype => "httpd_log_t",
            before  => Service["httpsd"],
        }
    }

    if $ssl_cert {
        $real_ssl_cert = $ssl_cert
    } else {
        $real_ssl_cert = "${puppet_ssldir}/certs/${homename}.pem"
    }

    file { "/etc/pki/tls/certs/${site_fqdn}.crt":
        ensure => present,
        source => $real_ssl_cert,
        mode   => "0644",
        owner  => root,
        group  => root,
        notify => Service["httpsd"],
    }

    if $ssl_key {
        $real_ssl_key = $ssl_key
    } else {
        $real_ssl_key = "${puppet_ssldir}/private_keys/${homename}.pem"
    }

    file { "/etc/pki/tls/private/${site_fqdn}.key":
        ensure => present,
        source => $real_ssl_key,
        mode   => "0600",
        owner  => root,
        group  => root,
        notify => Service["httpsd"],
    }

    if $ssl_chain {
        file { "/etc/pki/tls/certs/${site_fqdn}.chain.crt":
            ensure => present,
            source => $ssl_chain,
            mode   => "0644",
            owner  => root,
            group  => root,
            notify => Service["httpsd"],
        }
    }

    $site_conf = "/etc/httpd/site.https.d/${site_fqdn}.conf"
    $site_confdir = "/etc/httpd/site.https.d/${site_fqdn}.d"

    file { $site_conf:
        ensure  => present,
        content => template("apache/site.https.conf.erb"),
        mode    => "0644",
        owner   => root,
        group   => root,
        notify  => Service["httpsd"],
        require => [ File["/etc/pki/tls/certs/${site_fqdn}.crt"],
                     File["/etc/pki/tls/private/${site_fqdn}.key"], ],
    }

    file { $site_confdir:
        ensure  => directory,
        mode    => "0644",
        owner   => root,
        group   => root,
        purge   => true,
        force   => true,
        recurse => true,
        source  => [ "puppet:///files/apache/sslsites/${site_fqdn}",
                     "puppet:///modules/custom/empty", ],
        before  => File[$site_conf],
        notify  => Service["httpsd"],
    }

}


define apache::redhat::configfile($source, $content, $http, $https) {

    if defined(Class["apache::server"]) {
        file { "/etc/httpd/conf.http.d/${name}":
            ensure => $http ? {
                true    => present,
                default => absent,
            },
            mode   => "0644",
            owner  => root,
            group  => root,
            notify => Service["httpd"],
        }
        if $source {
            File["/etc/httpd/conf.http.d/${name}"] {
                source => $source,
            }
        }
        if $content {
            File["/etc/httpd/conf.http.d/${name}"] {
                content => $content,
            }
        }
        if ! $source and ! $content {
            File["/etc/httpd/conf.http.d/${name}"] {
                source => "/etc/httpd/conf.d/${name}",
            }
        }
        if $require {
            File["/etc/httpd/conf.http.d/${name}"] {
                require => $require,
            }
        }
    }

    if defined(Class["apache::sslserver"]) {
        file { "/etc/httpd/conf.https.d/${name}":
            ensure => $https ? {
                true    => present,
                default => absent,
            },
            mode   => "0644",
            owner  => root,
            group  => root,
            notify => Service["httpsd"],
        }
        if $source {
            File["/etc/httpd/conf.https.d/${name}"] {
                source => $source,
            }
        }
        if $content {
            File["/etc/httpd/conf.https.d/${name}"] {
                content => $content,
            }
        }
        if ! $source and ! $content {
            File["/etc/httpd/conf.https.d/${name}"] {
                source => "/etc/httpd/conf.d/${name}",
            }
        }
        if $require {
            File["/etc/httpd/conf.https.d/${name}"] {
                require => $require,
            }
        }
    }

}
