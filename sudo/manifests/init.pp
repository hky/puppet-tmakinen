# Install sudo and sudoers file.
#
class sudo {

    if $operatingsystem != "OpenBSD" {
        package { "sudo":
            ensure => installed,
            before => File["/etc/sudoers.d"],
        }
    }

    file { "/etc/sudoers.d":
        ensure  => directory,
        mode    => "0440",
        owner   => "root",
        group   => $operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
    }

    file { "/etc/sudoers":
        ensure  => present,
        mode    => "0440",
        owner   => "root",
        group   => $operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source  => "puppet:///modules/sudo/sudoers",
        require => File["/etc/sudoers.d"],
    }

}

# Add sudoer.
#
# === Parameters
#
#   $name:
#       User or group. Prefix group name with %.
#   $where:
#       Defaults to ALL.
#   $as_whom:
#       Defaults to ALL.
#   $what:
#       Defaults to ALL.
#
define sudo::sudoer($where="ALL", $as_whom="ALL", $what="ALL") {

    file { "/etc/sudoers.d/${name}":
        ensure  => present,
        mode    => "0440",
        owner   => "root",
        group   => $operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => "${name}\t${where}=(${as_whom})\t${what}\n",
        require => File["/etc/sudoers"],
    }

}
