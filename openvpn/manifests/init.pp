# Install openvpn client
#
class openvpn::client {

    package { "openvpn":
        ensure => installed,
    }

    service { "openvpn":
        ensure  => running,
        enable  => true,
        require => [ Package["openvpn"],
                     File["/etc/openvpn/openvpn.conf"],
                     File["/etc/openvpn/ta.key"],
                     File["/etc/openvpn/ca.crt"], ],
    }

    if !$openvpn_server {
        $openvpn_server = "127.0.0.1"
    }
    if !$openvpn_port {
        $openvpn_port = "1194"
    }
    if !$openvpn_ca {
        $openvpn_ca = "ca.crt"
    }
    if !$openvpn_ta {
        $openvpn_ta = "ta.key"
    }

    file { "/etc/openvpn/openvpn.conf":
        ensure  => present,
        content => template("openvpn/openvpn.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => "root",
        notify  => Service["openvpn"],
        require => Package["openvpn"],
    }
    file { "/etc/openvpn/ta.key":
        ensure  => present,
        source  => "puppet:///files/openvpn/ta.key",
        mode    => "0640",
        owner   => "root",
        group   => "root",
        require => Package["openvpn"],
    }
    file { "/etc/openvpn/ca.crt":
        ensure  => present,
        source  => "puppet:///files/openvpn/ca.crt",
        mode    => "0640",
        owner   => "root",
        group   => "root",
        require => Package["openvpn"],
    }
}
