# Enable firewall and install defined rules
#
# Rules are readed from variable $firewall_rules which needs to be an
# array containing list of opened services in format:
#
#     <proto>/<port> [source]
#
# for example:
#
#     $firewall_rules = [ "tcp/22 192.168.1.0/24",
#                         "tcp/80", ]
#
# If source is left out the service will be opened to all connecting
# hosts.
#
# Custom rules can be defined into variable $firewall_custom:
#
#     $firewall_custom = [ "pass in quick carp", ]
#
# Loading of extra modules is supported on centos. For example FTP
# support for iptables:
#
#     $firewall_modules = [ "nf_conntrack_ftp", ]

class firewall {

    if ! $firewall_custom {
        $firewall_custom = []
    }
    if ! $firewall_rules {
        $firewall_rules = []
    }
    if ! $firewall_modules {
        $firewall_modules = []
    }

    case $operatingsystem {
        centos,debian,fedora,ubuntu: {
            include firewall::iptables
        }
        openbsd: {
            include firewall::pf
        }
        default: {
            fail("Firewall module not supported in ${operatingsystem}")
        }
    }

}


# Enable firewall and install custom config file
#
# Config file is searched in following order:
#
#     puppet:///files/firewall/${config}.${fqdn}
#     puppet:///files/firewall/${config}
#
# where config is firewall configuration file name
# (iptables or pf.conf).
#
class firewall::custom {

    case $operatingsystem {
        centos,debian,fedora,ubuntu: {
            include firewall::custom::iptables
        }
        openbsd: {
            include firewall::custom::pf
        }
        default: {
            fail("Firewall module not supported in ${operatingsystem}")
        }
    }

}


# Linux iptables handler.
#
class firewall::common::iptables {

    package { "iptables":
        name   => $operatingsystem ? {
            centos => [ "iptables", "iptables-ipv6" ],
            debian => [ "iptables", "iptables-persistent" ],
            fedora => $operatingsystemrelease ? {
                /^1[0-5]/ => [ "iptables", "iptables-ipv6" ],
                default   => "iptables",
            },
            ubuntu => [ "iptables", "iptables-persistent" ],
        },
    }

    file { "/etc/sysconfig/iptables":
        name    => $operatingsystem ? {
            debian  => "/etc/iptables/rules",
            ubuntu  => "/etc/iptables/rules",
            default => "/etc/sysconfig/iptables",
        },
        ensure  => present,
        mode    => "0600",
        owner   => "root",
        group   => "root",
        require => Package["iptables"],
        notify  => Service["iptables"],
    }

    case $operatingsystem {
        centos,fedora: {
            $ip6states = versioncmp($kernelversion, "2.6.20")
            file { "/etc/sysconfig/ip6tables":
                ensure  => present,
                mode    => "0600",
                owner   => "root",
                group   => "root",
                require => Package["iptables"],
                notify  => Service["ip6tables"],
            }
            service { "ip6tables":
                ensure     => running,
                enable     => true,
                hasstatus  => true,
                hasrestart => true,
                require    => Package["iptables"],
            }
            if $firewall_modules {
                $firewall_modules_str = inline_template('\'"<%= @firewall_modules.join(" ") -%>"\'')
                augeas { "iptables-config":
                    context => "/files/etc/sysconfig/iptables-config",
                    changes => [ "set IPTABLES_MODULES ${firewall_modules_str}" ],
                    notify  => Service["iptables"],
                }
            }
        }
    }

    service { "iptables":
        name       => $operatingsystem ? {
            debian  => "iptables-persistent",
            ubuntu  => "iptables-persistent",
            default => "iptables",
        },
        ensure     => running,
        enable     => true,
        hasrestart => $operatingsystem ? {
            centos => true,
            debian => false,
            fedora => true,
            ubuntu => false,
        },
        status     => "iptables -t filter --list --line-numbers | egrep '^1'",
        require    => Package["iptables"],
    }

}


# Linux iptables handler to install default firewall config.
#
class firewall::iptables inherits firewall::common::iptables {

    File["/etc/sysconfig/iptables"] {
        content => template("firewall/iptables.erb"),
    }

    case $operatingsystem {
        centos,fedora: {
            File["/etc/sysconfig/ip6tables"] {
                content => template("firewall/ip6tables.erb"),
            }
        }
    }

}


# Linux iptables handler to install custom firewall config.
#
class firewall::custom::iptables inherits firewall::common::iptables {

    File["/etc/sysconfig/iptables"] {
        source  => [ "puppet:///files/firewall/iptables.${fqdn}",
                     "puppet:///files/firewall/iptables", ],
    }

}


# OpenBSD Packet Filter handler
#
class firewall::common::pf {

    file { "/etc/pf.conf":
        ensure  => present,
        mode    => "0600",
        owner   => "root",
        group   => "wheel",
        notify  => Exec["pfctl -f /etc/pf.conf"],
    }

    exec { "pfctl -f /etc/pf.conf":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
    }

}


# OpenBSD Packet Filter handler for default config.
#
class firewall::pf inherits firewall::common::pf {

    File["/etc/pf.conf"] {
        content => template("firewall/pf.conf.erb"),
    }

}


# OpenBSD Packet Filter handler for custom config.
#
class firewall::custom::pf inherits firewall::common::pf {

    File["/etc/pf.conf"] {
        source  => [ "puppet:///files/firewall/pf.conf.${fqdn}",
                     "puppet:///files/firewall/pf.conf", ],
    }

}


# OpenBSD NAT handler for FTP protocol.
#
class firewall::ftpproxy {

    service { "ftpproxy":
        ensure => running,
        enable => true,
        binary => "/usr/sbin/ftp-proxy",
        start  => "/usr/sbin/ftp-proxy",
    }

}
