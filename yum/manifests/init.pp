
# Install yum-updatesd service
#
class yum::updatesd {

    package { "yum-updatesd":
        ensure => installed,
    }

    file { "/etc/yum/yum-updatesd.conf":
        ensure => present,
        source => [ "puppet:///files/yum/yum-updatesd.conf.${fqdn}",
                    "puppet:///files/yum/yum-updatesd.conf",
                    "puppet:///modules/yum/yum-updatesd.conf", ],
        mode   => "0644",
        owner  => "root",
        group  => "root",
        notify => Service["yum-updatesd"],
    }

    service { "yum-updatesd":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package["yum-updatesd"],
    }

}


# Cron job for automatic downloading of updates
#
class yum::cron::download {

    include yum::plugin::downloadonly

    cron { "yum-cron-download":
        ensure  => present,
        command => "yum -d 0 -e 0 -y --downloadonly update",
        user    => "root",
        hour    => 3,
        minute  => fqdn_rand(60),
        require => Package["yum-plugin-downloadonly"],
    }

}

# Install changelog plugin
#
class yum::plugin::changelog {

    package { "yum-plugin-changelog":
        ensure => installed,
        name   => $operatingsystem ? {
            "centos" => $operatingsystemrelease ? {
                /^[1-5]/ => "yum-changelog",
                default  => "yum-plugin-changelog",
            },
            default  => "yum-plugin-changelog",
        },
    }

}


# Install downloadonly plugin
#
class yum::plugin::downloadonly {

    package { "yum-plugin-downloadonly":
        ensure => installed,
        name   => $operatingsystem ? {
            "centos" => $operatingsystemrelease ? {
                /^[1-5]/ => "yum-downloadonly",
                default  => "yum-plugin-downloadonly",
            },
            default  => "yum-plugin-downloadonly",
        },
    }

}


# Common prequisites for yum
#
class yum::common {

    case $operatingsystem {
        "fedora": {
            $osname = "fedora"
            $osver = $operatingsystemrelease
        }
        "centos": {
            $osname = "el"
            $osver = regsubst($operatingsystemrelease, '^(\d+)\..*$', '\1')
        }
    }

}


# Manage yum excludes.
#
# === Global variables
#
#   $yum_exclude:
#       Array of packets to exclude.
#
class yum::exclude {

    if !$yum_exclude {
        fail("\$yum_exclude must be defined for yum::exclude")
    }

    $yum_exclude_real = inline_template('<%= yum_exclude.join(" ") -%>')

    augeas { "yum-exclude":
        context => "/files/etc/yum.conf/main",
        changes => "set exclude '${yum_exclude_real}'",
    }

}


# Add new yum repository.
#
# === Parameters
#
#   $name:
#       Repository name.
#   $ensure:
#       Present/enable enables repository (default), absent removes
#       repository completely and disable disables repository.
#   $baseurl:
#       Base URL for this repository.
#   $mirrorlist:
#       Mirrorlist URL for this repository.
#   $descr:
#       Repository description. Defaults to $name.
#   $gpgkey:
#       Location where GPG signing key can be found. If not set
#       GPG check will be disabled.
#
# === Sample usage
#
# yum::repo { "tmz-puppet":
#     baseurl => "http://tmz.fedorapeople.org/repo/puppet/epel/5/i386/",
#     descr   => "Puppet for EL 5 - i386",
#     gpgkey  => "http://tmz.fedorapeople.org/repo/RPM-GPG-KEY-tmz",
# }
#
define yum::repo($ensure="present", $baseurl="", $mirrorlist="", $descr="", $gpgkey="") {

    tag("bootstrap")

    # can't use ensure variable inside template :(
    $status = $ensure

    if $status != "absent" {

        if !$baseurl and !$mirrorlist {
            fail("Either \$baseurl or \$mirrorlist needs to be defined for yum::repo")
        }
        if $baseurl and $mirrorlist {
            fail("Cannot set both \$baseurl and \$mirrorlist for yum::repo")
        }

        if regsubst($gpgkey, "^(puppet://).*", '\1') == "puppet://" {
            file { "/etc/pki/rpm-gpg/RPM-GPG-KEY-${name}":
                ensure => present,
                source => $gpgkey,
                mode   => "0644",
                owner  => "root",
                group  => "root",
                before => File["/etc/yum.repos.d/${name}.repo"],
                notify => Exec["rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-${name}"],
            }
            exec { "rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-${name}":
                path        => "/bin:/usr/bin:/sbin:/usr/sbin",
                user        => "root",
                refreshonly => true,
            }
            $gpgkey_real = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-${name}"
        } else {
            $gpgkey_real = $gpgkey
        }

        if $descr {
            $descr_real = $descr
        } else {
            $descr_real = $name
        }

    }

    file { "/etc/yum.repos.d/${name}.repo":
        ensure  => $status ? {
            "absent" => absent,
            default  => present,
        },
        content => template("yum/yum.repo.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
    }

}


# Add Adobe repository
#
class yum::repo::adobe {

    yum::repo { "adobe":
        descr   => "Adobe Systems Incorporated",
        baseurl => "http://linuxdownload.adobe.com/linux/i386/",
        gpgkey  => "puppet:///modules/yum/keys/adobe.key",
    }

    if $architecture == "x86_64" {
        yum::repo { "adobe-x86_64":
            descr   => "Adobe Systems Incorporated (x86_64)",
            baseurl => "http://linuxdownload.adobe.com/linux/x86_64/",
            gpgkey  => "puppet:///modules/yum/keys/adobe.key",
        }
    }

}


# Add CentOS CR (continuous release) repository
#
class yum::repo::centos-cr {

    tag("bootstrap")

    if $operatingsystem != "CentOS" {
        fail("CentOS CR repository not supported in ${operatingsystem}")
    }

    package { "centos-release-cr":
        ensure => installed,
    }

}


class yum::repo::elrepo {

    include yum::common

    yum::repo { "elrepo":
        descr      => "ELRepo.org Community Enterprise Linux Repository",
        mirrorlist => "http://elrepo.org/mirrors-elrepo.el${yum::common::osver}",
        gpgkey     => "puppet:///modules/yum/keys/elrepo.key",
    }

}


# Add Fedora EPEL repository
#
class yum::repo::epel {

    include yum::common

    if $yum::common::osname != "el" {
        fail("EPEL repository not supported in ${::operatingsystem}")
    }

    yum::repo { "epel":
        descr      => "Extra Packages for Enterprise Linux ${yum::common::osver} - \$basearch",
        mirrorlist => "http://mirrors.fedoraproject.org/mirrorlist?repo=epel-${yum::common::osver}&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/epel${yum::common::osver}.key",
    }

}


# Add Fedora EPEL Testing repository
#
class yum::repo::epel-testing {

    include yum::common

    if $yum::common::osname != "el" {
        fail("EPEL Testing repository not supported in ${::operatingsystem}")
    }

    yum::repo { "epel-testing":
        descr      => "Extra Packages for Enterprise Linux ${yum::common::osver} - Testing - \$basearch",
        mirrorlist => "http://mirrors.fedoraproject.org/mirrorlist?repo=testing-epel${yum::common::osver}&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/epel${yum::common::osver}.key",
    }

}


# Add Google repository
#
class yum::repo::google {

    yum::repo { "google":
        descr   => "Google - \$basearch",
        baseurl => "http://dl.google.com/linux/rpm/stable/\$basearch",
        gpgkey  => "puppet:///modules/yum/keys/google.key",
    }

}


# Add RPM Fusion Free repository
#
class yum::repo::rpmfusion-free {

    include yum::common

    yum::repo { "rpmfusion-free":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Free",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=free-${yum::common::osname}-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-free-${yum::common::osname}-${yum::common::osver}.key",
    }
    yum::repo { "rpmfusion-free-updates":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Free",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=free-${yum::common::osname}-updates-released-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-free-${yum::common::osname}-${yum::common::osver}.key",
    }

}


# Add RPM Fusion Free Testing repository
#
class yum::repo::rpmfusion-free-testing {

    include yum::repo::rpmfusion-free

    yum::repo { "rpmfusion-free-updates-testing":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Free - Test Updates",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=free-${yum::common::osname}-updates-testing-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-free-${yum::common::osname}-${yum::common::osver}.key",
    }

}


# Add RPM Fusion Nonfree repository
#
# Enabling this will also enable RPM Fusion Free repository.
#
class yum::repo::rpmfusion-nonfree {

    include yum::common
    include yum::repo::rpmfusion-free

    yum::repo { "rpmfusion-nonfree":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Nonfree",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-${yum::common::osname}-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-nonfree-${yum::common::osname}-${yum::common::osver}.key",
    }
    yum::repo { "rpmfusion-nonfree-updates":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Nonfree",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-${yum::common::osname}-updates-released-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-nonfree-${yum::common::osname}-${yum::common::osver}.key",
    }

}


# Add RPM Fusion Nonfree Testing repository
#
# Enabling this will also enable RPM Fusion Free repository.
#
class yum::repo::rpmfusion-nonfree-testing {

    include yum::repo::rpmfusion-nonfree
    include yum::repo::rpmfusion-free-testing

    yum::repo { "rpmfusion-nonfree-updates-testing":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Nonfree - Test Updates",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-${yum::common::osname}-updates-testing-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-nonfree-${yum::common::osname}-${yum::common::osver}.key",
    }

}


# Add Skype repository
#
class yum::repo::skype {

    yum::repo { "skype":
        descr   => "Skype Yum Repository",
        baseurl => "http://download.skype.com/linux/repos/fedora/updates/i586/",
    }

}

# Add Puppetlabs repository
#
class yum::repo::puppetlabs {

    case $::operatingsystem {
        "fedora": {
            $dir = "fedora/f\$releasever/products/\$basearch"
        }
        "centos": {
            $dir = "el/\$releasever/products/\$basearch"
        }
        default: {
            fail("yum::repo::puppetlabs not supported in ${::operatingsystem}")
        }
    }

    yum::repo { "puppetlabs":
        descr   => "Puppet Labs Packages",
        baseurl => "http://yum.puppetlabs.com/${dir}",
        gpgkey  => "http://yum.puppetlabs.com/RPM-GPG-KEY-puppetlabs",
    }

}

