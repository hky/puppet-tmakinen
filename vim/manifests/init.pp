
# Install Vim (Vi IMproved)
#
class vim {

    package { "vim":
        name   => $::operatingsystem ? {
            "centos" => "vim-enhanced",
            "fedora" => "vim-enhanced",
            default  => "vim",
        },
        ensure => installed,
    }

    if $::puppetversion =~ /^2\./ {
        Package["vim"] {
            flavor => "gtk2",
        }
    }

}
