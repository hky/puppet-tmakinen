# Install libvirt software.
#
class libvirt::client {

    include user::system
    realize(User["qemu"], Group["qemu"])

    file { "/var/lib/qemu":
        ensure  => directory,
        mode    => "0700",
        owner   => "qemu",
        group   => "qemu",
        require => [ User["qemu"], Group["qemu"], ],
    }

    case $operatingsystem {
        centos,fedora: {
            package { [ "libvirt", "virt-manager", "virt-viewer", ]:
                ensure  => installed,
                require => File["/var/lib/qemu"],
            }
        }
        default: {
            fail("Not supported on ${operatingsystem}.")
        }
    }

}


# Install and configure KVM and libvirtd.
#
# === Global variables
#
#   $libvirt_admingroup:
#       Group which has access to system libvirtd.
#
class libvirt::kvm inherits libvirt::client {

    case $operatingsystem {
        centos,fedora: {
            case $operatingsystemrelease {
                /5\..*/: {
                    package { ["kvm", "kmod-kvm"]:
                        ensure  => installed,
                        before  => Service["libvirtd"],
                        require => [ User["qemu"], Group["qemu"] ],
                    }
                }
                default: {
                    package { "qemu-kvm":
                        ensure  => installed,
                        before  => Service["libvirtd"],
                        require => [ User["qemu"], Group["qemu"] ],
                    }
                    package { "ruby-libvirt":
                        ensure => installed,
                    }
                }
            }
        }
        default: {
            fail("Operating system not supported")
        }
    }

    if !$libvirt_admingroup {
        $libvirt_admingroup = "root"
    }

    file { "/etc/libvirt/libvirtd.conf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("libvirt/libvirtd.conf.erb"),
        require => Package["libvirt"],
        notify  => Service["libvirtd"],
    }

    service { "libvirtd":
        ensure => running,
        enable => true,
    }

}

