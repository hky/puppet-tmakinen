
# Install sasl client
#
class sasl::client {

    package { "cyrus-sasl":
        ensure => installed,
    }

    if $kerberos_realm {
        package { "cyrus-sasl-gssapi":
            ensure => installed,
        }
    }

}

# Install saslauthd daemon.
#
# === Global variables
#
#   $saslauthd_mech:
#       Authentication mechanism to use. Defaults to system
#       default. Supported mechanisms include pam, ldap and kerberos5.
#
#       For ldap authentication, see ldap::client for required global variables.
#  
class sasl::saslauthd {

    require sasl::client

    case $saslauthd_mech {
        "","pam": { }
        "ldap": {
            include ldap::client
            
            augeas { "set-saslauthd-mech":
                context => "/files/etc/sysconfig/saslauthd",
                changes => "set MECH ldap",
                notify  => Service["saslauthd"],
            }
            
            file { "/etc/saslauthd.conf":
                ensure => present,
                mode => 0644,
                owner => "root",
                group => "root",
                content => template("sasl/saslauthd.conf.ldap.erb"),
                notify  => Service["saslauthd"],                
            }
        }
        "kerberos5": {
            augeas { "set-saslauthd-mech":
                context => "/files/etc/sysconfig/saslauthd",
                changes => "set MECH kerberos5",
                notify  => Service["saslauthd"],
            }
        }
        default: {
            fail("Unknown mechanism ${saslauthd_mech} for sasl::saslauthd")
        }
    }

    service { "saslauthd":
        ensure  => running,
        enable  => true,
    }

    file { "/etc/sasldb2":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Exec["generate-sasldb2"],
        before  => Service["saslauthd"],
    }
    exec { "generate-sasldb2":
        command => "saslpasswd2 -d foobar ; true",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        creates => "/etc/sasldb2",
    }

}


# Install saslauthd service config
#
# === Parameters
#
#   $name:
#       Service name.
#
# === Sample usage
#
# sasl::saslauthd::service { "Sendmail": }
#
define sasl::saslauthd::service() {

    case $architecture {
        "i386":   { $libdir = "/usr/lib/sasl2" }
        "x86_64": { $libdir = "/usr/lib64/sasl2" }
        default:  { fail("Unknown architecture ${architecture}") }
    }

    file { "${libdir}/${name}.conf":
        ensure  => present,
        source  => [ "puppet:///files/sasl/${name}.${fqdn}.conf",
                     "puppet:///files/sasl/${name}.conf",
                     "puppet:///files/sasl/service.conf",
                     "puppet:///modules/sasl/service.conf", ],
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Service["saslauthd"],
    }

}

