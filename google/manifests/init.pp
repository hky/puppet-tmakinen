# Install common prequisites for Google products.
#
class google::common {

    case $operatingsystem {
        "centos","fedora": {
            include yum::repo::google
        }
        default: {
            fail("Google products via puppet not supported on ${operatingsystem}")
        }
    }

}


# Install Google Chrome browser.
#
class google::chrome {

    include google::common
    package { "google-chrome-beta":
        ensure  => installed,
        require => Class["google::common"],
    }

}


# Install Google Desktop.
#
class google::desktop {

    include google::common
    package { "google-desktop-linux":
        ensure  => installed,
        require => Class["google::common"],
    }

}
