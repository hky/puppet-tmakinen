import "dovecot1.pp" # Dovecot v1.x
import "dovecot2.pp" # Dovecot v2.x

class dovecot::common {

    case $operatingsystem {
        centos,fedora: {
            package { ["dovecot"]:
                ensure => installed,
            }
        }
        default: {
            fail("Dovecot module not supported in ${operatingsystem}.")
        }
    }
}

# === Global variables
#
#   $dovecot_mail_domain:
#       Mail domain name.
#   $dovecot_ssl_csr:
#       Puppet source for the CSR file.
#   $dovecot_ssl_cert:
#       Puppet source for the X.509 certificate.
#   $dovecot_ssl_key:
#       Puppet source for the X.509 key.
#   $dovecot_ssl_ca:
#       Puppet source for the optional X.509 ca certificate.
#   $dovecot_mailbox_format:
#       Mailbox format to use in user's homedir ["mbox" | "mdbox"]
#   $dovecot_zlib:
#       Compress mailboxes with zlib ["yes" | "no"]
class dovecot::server inherits dovecot::common {

    if ! $dovecot_mailbox_format {
        $dovecot_mailbox_format = "mbox"
    }

    case $operatingsystem {
        centos,fedora: {
            $dovecot_ssl_dir = "/etc/pki/tls"

            case $operatingsystemrelease {
                /^6\./: {
                    include dovecot::server::v2
                }
                default: {
                    include dovecot::server::v1
                }
            }
        }
        default: {
            fail("Dovecot module not supported in ${operatingsystem}.")
        }
    }


}
