class networker::client {

    package { [ "lgtoclnt", "lgtoman" ]:
        ensure => installed,
    }

    file { [ "/nsr", "/nsr/res" ]:
        ensure  => directory,
        mode    => "0755",
        owner   => root,
        group   => root,
        require => Package["lgtoclnt"],
    }

    file { "/nsr/res/servers":
        ensure  => present,
        content => template("networker/servers.erb"),
        mode    => "0644",
        owner   => root,
        group   => root,
        require => File["/nsr/res"],
        notify  => Service["networker"],
    }

    service { "networker":
        ensure    => running,
        enable    => true,
        hasstatus => true,
    }

    exec { "nsrports -S 7937-7940":
        path   => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless => "nsrports | egrep '^Service ports: 7937-7940[[:space:]]$'",
    }

}
