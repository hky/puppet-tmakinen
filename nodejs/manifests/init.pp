# Install node.js.
#
class nodejs {

    case $::operatingsystem {
        "centos": {
            yum::repo { "nodejs":
                mirrorlist => "http://nodejs.tchol.org/mirrors/nodejs-stable-el\$releasever",
                gpgkey     => "http://nodejs.tchol.org/stable/RPM-GPG-KEY-tchol",
            }

            package { [ "nodejs", "nodejs-compat-symlinks", "npm" ]:
                ensure  => installed,
                require => Yum::Repo["nodejs"],
            }
        }
        default: {
            fail("nodejs not supported on ${::operatingsystem}")
        }
    }

}
