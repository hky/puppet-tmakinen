
# Install netcat
#
class netcat {

    if $::operatingsystem != "OpenBSD" {
        package { "netcat":
            name   => $::operatingsystem ? {
                "ubuntu" => "netcat",
                default  => "nc",
            },
            ensure => present,
        }
    }

}
