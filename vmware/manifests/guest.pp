
# Install VMware optimizations if running as guest.
#
class vmware::guest {

    case $virtual {
        vmware: {
            case $operatingsystem {
                centos: { include vmware::guest::centos }
            }
        }
    }

}


# Handler for CentOS.
#
class vmware::guest::centos {

    yum::repo { "vmware-tools":
        descr   => "VMware Tools Repository",
        baseurl => "http://packages.vmware.com/tools/esx/5.0/rhel\$releasever/\$basearch",
    }

    package { "VMwareTools":
        ensure => absent,
    }

    package { [ "vmware-tools-esx-nox",
                "vmware-tools-esx-kmods", ]:
        ensure  => installed,
        require => [ Package["VMwareTools"],
                     Yum::Repo["vmware-tools"], ],
    }

}
