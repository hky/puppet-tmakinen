
# Install Git client tools.
#
class git::client {

    package { "git":
        name   => $operatingsystem ? {
            "ubuntu" => "git-core",
            default  => "git",
        },
        ensure => installed,
    }

}


# Install prequisites for serving Git repositories
#
# === Global variables
#
#   $git_datadir:
#       Directory where repositories are stored
#
class git::server {

    include git::client

    if $git_datadir {
        file { $git_datadir:
            ensure => directory,
            mode   => "0755",
            owner  => "root",
            group  => "root",
        }
        file { "/srv/git":
            ensure  => link,
            target  => $git_datadir,
            require => File[$git_datadir],
        }
    } else {
        file { "/srv/git":
            ensure  => directory,
            mode    => "0755",
            owner   => "root",
            group   => "root",
            seltype => "httpd_sys_content_t",
        }
    }

    if "${selinux}" == "true" {
        selinux::manage_fcontext { "/srv/git(/.*)?":
            type   => "httpd_sys_content_t",
            before => File["/srv/git"],
        }
        if $git_datadir {
            selinux::manage_fcontext { "${git_datadir}(/.*)?":
                type   => "httpd_sys_content_t",
                before => File[$git_datadir],
            }
        }
    }

}


# Install Git daemon
#
class git::daemon inherits git::server {

    package { "git-daemon":
        ensure => installed,
    }

    include inetd::server

    inetd::service { "git":
        ensure  => present,
        require => [ File["/srv/git"],
                     Package["git-daemon"] ],
    }

    case $operatingsystem {
        "centos","fedora": {
            file { "/var/lib/git":
                ensure  => link,
                force   => true,
                target  => "/srv/git",
                owner   => "root",
                group   => "root",
                require => File["/srv/git"],
            }
        }
    }

}


# Install gitweb
#
class git::gitweb inherits git::server {

    package { "gitweb":
        ensure => installed,
    }

    file { "/var/www/git/gitweb_config.perl":
        ensure  => present,
        source  => [ "puppet:///files/git/gitweb_config.perl.${fqdn}",
                     "puppet:///files/git/gitweb_config.perl",
                     "puppet:///modules/git/gitweb_config.perl", ],
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["gitweb"],
    }

}
