# Install samba server.
#
class samba::server {

    package { "samba":
        ensure => installed,
    }

    case $operatingsystem {
        openbsd: {
            service { "nmbd":
                ensure     => running,
                enable     => true,
                binary     => "/usr/local/libexec/nmbd",
                start      => "/usr/local/libexec/nmbd -D",
                require    => File["/etc/samba/smb.conf"],
                subscribe  => File["/etc/samba/smb.conf"],
            }
            service { "smbd":
                ensure     => running,
                enable     => true,
                binary     => "/usr/local/libexec/smbd",
                start      => "/usr/local/libexec/smbd -D",
                require    => File["/etc/samba/smb.conf"],
                subscribe  => File["/etc/samba/smb.conf"],
            }
        }
        centos: {
            case $operatingsystemrelease {
                /^[1-5]\./: {
                    service { "smb":
                        ensure     => running,
                        enable     => true,
                        hasstatus  => true,
                        hasrestart => true,
                        require    => File["/etc/samba/smb.conf"],
                        subscribe  => File["/etc/samba/smb.conf"],
                    }
                }
                default: {
                    service { [ "smb", "nmb" ]:
                        ensure     => running,
                        enable     => true,
                        hasstatus  => true,
                        hasrestart => true,
                        require    => File["/etc/samba/smb.conf"],
                        subscribe  => File["/etc/samba/smb.conf"],
                    }
                }
            }
        }
        fedora: {
            service { [ "smb", "nmb" ]:
                ensure     => running,
                enable     => true,
                hasstatus  => true,
                hasrestart => true,
                require    => File["/etc/samba/smb.conf"],
                subscribe  => File["/etc/samba/smb.conf"],
            }
        }
        ubuntu: {
            service { [ "smbd", "nmbd" ]:
                ensure     => running,
                enable     => true,
                hasstatus  => true,
                hasrestart => true,
                require    => File["/etc/samba/smb.conf"],
                subscribe  => File["/etc/samba/smb.conf"],
            }
        }
        default: {
            fail("samba::server not supported in '${operatingsystem}'")
        }
    }

    file { "/etc/samba/smb.conf":
        ensure  => present,
        source  => [ "puppet:///files/samba/smb.conf.${fqdn}",
                     "puppet:///files/samba/smb.conf", ],
        mode    => "0644",
        owner   => "root",
        group   => $operatingsystem ? {
            openbsd => "wheel",
            default => "root",
        },
        require => Package["samba"],
    }

    file { "/etc/samba/lmhosts":
        ensure  => present,
        source  => [ "puppet:///files/samba/lmhosts.${fqdn}",
                     "puppet:///files/samba/lmhosts",
                     "puppet:///modules/samba/lmhosts", ],
        mode    => "0644",
        owner   => "root",
        group   => $operatingsystem ? {
            openbsd => "wheel",
            default => "root",
        },
        require => Package["samba"],
    }

}


# Install support scripts for Backup Domain Controller
#
class samba::bdc {

    include samba::server

    file { "/etc/cron.hourly/sync-netlogon.sh":
        ensure => present,
        source => "puppet:///modules/samba/sync-netlogon.sh",
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }

}


# Join samba server into domain.
#
# === Global variables
#
#   $samba_join_user:
#       Username to use when joining domain.
#
#   $samba_join_pass:
#       Password to use when joining domain.
#
class samba::domainmember {

    include samba::server

    exec { "net join":
        command     => "net join -U ${samba_join_user}%\"\${SECRET}\"",
        environment => "SECRET=${samba_join_pass}",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless      => "net rpc testjoin -U ${samba_join_user}%\"\${SECRET}\"",
        require     => Service["smb"],
    }

}


# Set LDAP auth password to samba.
#
# === Global variables
#
#   $samba_ldap_pass:
#       Password to set in samba secrets.
#
class samba::ldap {

    include samba::server

    exec { "smbpasswd -w":
        command     => "smbpasswd -w \"\${SECRET}\"",
        environment => "SECRET=${samba_ldap_pass}",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless      => "tdbtool /etc/samba/secrets.tdb keys | fgrep 'SECRETS/LDAP_BIND_PW/'",
        require     => File["/etc/samba/smb.conf"],
        notify      => Service["smb"],
    }

}
