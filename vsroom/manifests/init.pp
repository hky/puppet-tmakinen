# Install standalone vsroom.
#
# === Global variables
#
#   $vsroom_authurl:
#       Authentication path. Defaults to "../common/auth_credentials.php".
#
#   $vsroom_boshurl:
#       XMPP BOSH path. Defaults to "/bosh/".
#
class vsroom {

    if !$vsroom_authurl {
        $vsroom_authurl = "../common/auth_credentials.php"
    }
    if !$vsroom_boshurl {
        $vsroom_boshurl = "/bosh/"
    }

    include vsroom::common

}


# Install vsroom with collab authentication.
#
class vsroom::collab {

    if !$vsroom_authurl {
        $vsroom_authurl = "/collab/auth_credentials.php"
    }
    if !$vsroom_boshurl {
        $vsroom_boshurl = "/bosh/"
    }

    include vsroom::common

    file { "/srv/wikis/collab/htdocs/auth_credentials.php":
        ensure  => present,
        mode    => "0660",
        owner   => "collab",
        group   => "collab",
        source  => "${vsroom::common::htdocs}/common/auth_credentials.php",
        require => [
          File["/srv/wikis/collab/htdocs"],
          Python::Setup::Install["/usr/local/src/vsroom"],
        ],
    }

}


# Install common vsroom components.
#
class vsroom::common {

    case $operatingsystem {
        centos: {
            case $operatingsystemrelease {
                /^5/: {
                    Python::Setup::Install["/usr/local/src/vsroom"] {
                        python  => "python2.6",
                        require => Package["python26"],
                    }
                }
            }
        }
    }

    if !$vsroom_package {
        if $vsroom_package_latest {
            $vsroom_package = $vsroom_package_latest
        } else {
            fail("Must define \$vsroom_package or \$vsroom_package_latest")
        }
    }

    file { "/usr/local/src/vsroom.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///files/packages/${vsroom_package}",
    }
    util::extract::tar { "/usr/local/src/vsroom":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/vsroom.tar.gz",
        require => File["/usr/local/src/vsroom.tar.gz"],
        before  => Python::Setup::Install["/usr/local/src/vsroom"],
    }
    python::setup::install { "/usr/local/src/vsroom": }

    include user::system
    realize(User["vsroom"], Group["vsroom"])

    if $vsroom_datadir {
        file { $vsroom_datadir:
            ensure  => directory,
            mode    => "2750",
            owner   => "root",
            group   => "vsroom",
        }

        file { "/var/lib/vsroom":
            ensure  => link,
            target  => $vsroom_datadir,
            require => File[$vsroom_datadir],
        }
    } else {
        file { "/var/lib/vsroom":
            ensure  => directory,
            mode    => "2750",
            owner   => "root",
            group   => "vsroom",
        }
    }

    $htdocs = $operatingsystem ? {
        "ubuntu" => "/usr/local/share/vsroom/htdocs",
        default  => "/usr/share/vsroom/htdocs",
    }

    file { "${htdocs}/config.json":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("vsroom/config.json.erb"),
        require => Python::Setup::Install["/usr/local/src/vsroom"],
    }

    define configwebhost() {
        file { "/srv/www/https/${name}/vsroom":
            ensure  => link,
            target  => $htdocs,
            require => File["/srv/www/https/${name}"],
        }
    }

    if $vsroom_webhosts {
        apache::configfile { "vsroom.conf":
            http   => false,
            source => "puppet:///modules/vsroom/vsroom-httpd.conf",
        }

        configwebhost { $vsroom_webhosts: }
    }

}
