# Install common prequisites for Adobe products.
#
class adobe::common {

    case $operatingsystem {
        "centos","fedora": {
            include yum::repo::adobe
        }
        "ubuntu": {
            include apt::repo::partner
        }
        default: {
            fail("Adobe products via puppet not supported on ${operatingsystem}")
        }
    }

}


# Intall Adobe AIR.
#
class adobe::air {

    include adobe::common
    package { "adobeair":
        ensure  => installed,
        require => Class["adobe::common"],
    }

}


# Install Adobe Flash plugin for browsers.
#
class adobe::flash {

    include adobe::common
    package { "adobe-flash-plugin":
        name    => $operatingsystem ? {
            "ubuntu" => "adobe-flashplugin",
            default  => "flash-plugin",
        },
        ensure  => installed,
        require => Class["adobe::common"],
    }

}


# Install Adobe Reader.
#
class adobe::reader {

    include adobe::common
    package { "acroread":
        name    => $operatingsystem ? {
            "ubuntu" => "acroread",
            default  => "AdobeReader_enu",
        },
        ensure  => installed,
        require => Class["adobe::common"],
    }

}
