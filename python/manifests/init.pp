
# Install python
#
class python {

    package { "python":
        ensure => installed,
        notify => $::operatingsystem ? {
            "openbsd" => Exec["python-links"],
            default   => undef,
        },
    }

    exec { "python-links":
        path        => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
        command     => "pkg_info python | egrep '^[ ]*ln -sf' | sh",
        user        => "root",
        group       => "wheel",
        refreshonly => true,
    }

}


# Install python software using setup.py.
#
# === Parameters
#
#   $name:
#       Source directory.
#   $python:
#       Python executable name. Defaults to python.
#
# === Sample usage
#
# python::setup::install { "/usr/local/src/moin-1.8.8": }
#
define python::setup::install($python="python") {

    exec { "python-setup-install-${name}":
        path    => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
        cwd     => $name,
        command => "/bin/sh -c 'umask 022; ${python} setup.py install'",
        creates => "${name}/build",
    }

}

# Compile python script into binary form.
#
# === Parameters
#
#   $name:
#       File name to compile.
#
# === Sample usage
#
# python::compile { "/usr/lib/python2.4/site-packages/dynldap.py": }
#
define python::compile() {

    exec { "python -c \"import py_compile; py_compile.compile('${name}')\"":
        path   => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
        onlyif => "test '${name}' -nt '${name}c'",
    }

}
