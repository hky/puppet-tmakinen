# Install abusehelper from svn.
#
class abusehelper {

    case $operatingsystem {
        ubuntu: {
            package { [ "python-pyme", "python-tz" ]:
                ensure => installed,
            }
        }
        centos: {
            case $operatingsystemrelease {
                /^5/: {
                    package { "python26":
                        ensure => installed,
                    }
                    Python::Setup::Install["/usr/local/src/abusehelper",
                                           "/usr/local/src/idiokit"] {
                        python  => "python2.6",
                        require => Package["python26"],
                    }
                }
            }
        }
    }

    if !$abusehelper_package {
        if $abusehelper_package_latest {
            $abusehelper_package = $abusehelper_package_latest
        } else {
            fail("Must define \$abusehelper_package or \$abusehelper_package_latest")
        }
    }

    if !$idiokit_package {
        if $idiokit_package_latest {
            $idiokit_package = $idiokit_package_latest
        } else {
            fail("Must define \$idiokit_package or \$idiokit_package_latest")
        }
    }

    file { "/usr/local/src/abusehelper.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => $operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source => "puppet:///files/packages/${abusehelper_package}",
    }
    util::extract::tar { "/usr/local/src/abusehelper":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/abusehelper.tar.gz",
        require => File["/usr/local/src/abusehelper.tar.gz"],
        before  => Python::Setup::Install["/usr/local/src/abusehelper"],
    }
    file { "/usr/local/src/idiokit.tar.gz":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => $operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source => "puppet:///files/packages/${idiokit_package}",
    }
    util::extract::tar { "/usr/local/src/idiokit":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/idiokit.tar.gz",
        require => File["/usr/local/src/idiokit.tar.gz"],
        before  => Python::Setup::Install["/usr/local/src/idiokit"],
    }
    python::setup::install { [ "/usr/local/src/abusehelper",
                               "/usr/local/src/idiokit", ]:
    }

    include user::system
    realize(User["abusehel"], Group["abusehel"])

    if $abusehelper_datadir {
        file { $abusehelper_datadir:
            ensure  => directory,
            mode    => "2750",
            owner   => "root",
            group   => "abusehel",
            require => User["abusehel"],
        }

        file { "/var/lib/ah2":
            ensure  => link,
            target  => $abusehelper_datadir,
            require => File[$abusehelper_datadir],
        }
    } else {
        file { "/var/lib/ah2":
            ensure  => directory,
            mode    => "2750",
            owner   => "root",
            group   => "abusehel",
            require => User["abusehel"],
        }
    }

}
