# Install time (time-stream) server.
#
class time::server {

    include inetd::server

    inetd::service { "time-stream":
        ensure => present,
    }

}

# Set default timezone
#
# $timezone_set should point to a file under /usr/share/zoneinfo/
# for example $timezone_set = "Europe/Helsinki".
class time::zone {
    file { "/etc/localtime":
        ensure => link,
        target => "/usr/share/zoneinfo/${timezone_set}",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

    case $operatingsystem {
        "centos","redhat": {
            file { "/etc/sysconfig/clock":
                ensure  => present,
                mode    => "0644",
                content => template("time/sysconfig_clock.erb"),
            }
        }
        "ubuntu": {
            file { "/etc/timezone":
                ensure  => present,
                mode    => "0644",
                content => "${timezone_set}\n",
            }
        }
        "openbsd": { } # file /etc/localtime is enough
        "default": {
            fail("time::zone not supported on ${::operatingsystem}")
        }
    }
}
