# Install python bindings for igraph.
#
class igraph::python {

    case $operatingsystem {
        ubuntu: {
            package { "python-igraph":
                ensure  => installed,
            }
        }
    }

}
