
# Install Ruby Mongrel packages.
#
class mongrel {

    require ruby::rubygems

    package { "mongrel":
        ensure   => installed,
        name     => $::operatingsystem ? {
            openbsd => "ruby-mongrel",
            debian  => "mongrel",
            ubuntu  => "mongrel",
            centos  => $::operatingsystemrelease ? {
                /^[1-5]/ => "rubygem-mongrel",
                default  => "mongrel",
            },
            default => "rubygem-mongrel",
        },
        provider => $::operatingsystem ? {
            centos  => $::operatingsystemrelease ? {
                /^[1-5]/ => undef,
                default  => "gem",
            },
            default => undef,
        },
    }

}
