# Install IRC server.
#
class irc::server {

    package { "ircd-ratbox":
        ensure => installed,
    }

    file { "/var/lib/ratbox":
        ensure  => directory,
        owner   => "irc",
        group   => "irc",
        mode    => "0700",
        require => Package["ircd-ratbox"],
    }

}


# Install IRC services.
#
class irc::services inherits irc::server {

    package { "ratbox-services-sqlite":
        ensure => installed,
    }

    file { "/var/lib/ratbox-services":
        ensure  => directory,
        owner   => "irc",
        group   => "irc",
        mode    => "0700",
        require => Package["ircd-ratbox", "ratbox-services-sqlite"],
    }

}


# Configure IRC network.
#
define irc::network($desc, $servername, $serverdesc,
                    $port, $sslport, $operpass, $userpass="",
                    $services=false, $servpass="") {

    include irc::server

    file { "/var/lib/ratbox/${name}":
        ensure  => directory,
        owner   => "irc",
        group   => "irc",
        mode    => "0700",
        require => File["/var/lib/ratbox"],
    }

    file { "/var/lib/ratbox/${name}/ircd.conf":
        ensure  => present,
        mode    => "0600",
        owner   => "irc",
        group   => "irc",
        content => $services ? {
            true  => template("irc/ircd-ratbox.conf.erb", "irc/ircd-ratbox-services.conf.erb"),
            false => template("irc/ircd-ratbox.conf.erb"),
        },
        before  => Service["ircd-${name}"],
        notify  => Service["ircd-${name}"],
        require => File["/var/lib/ratbox/${name}"],
    }

    ssl::certificate { "/var/lib/ratbox/${name}/ircd.pem":
        cn      => $servername,
        mode    => "0600",
        owner   => "irc",
        group   => "irc",
        require => File["/var/lib/ratbox/${name}"],
    }

    ssl::dhparam { "/var/lib/ratbox/${name}/dh.pem":
        mode    => "0600",
        owner   => "irc",
        group   => "irc",
        require => File["/var/lib/ratbox/${name}"],
    }

    file { "/etc/init.d/ircd-${name}":
        ensure  => present,
        mode    => "0755",
        owner   => root,
        group   => root,
        content => template("irc/ircd-ratbox.init.erb"),
        before  => Service["ircd-${name}"],
        notify  => Exec["enable-ircd-${name}"],
    }

    exec { "enable-ircd-${name}":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "update-rc.d ircd-${name} defaults",
        refreshonly => true,
        before      => Service["ircd-${name}"],
    }

    service { "ircd-${name}":
        ensure  => running,
        enable  => true,
        status  => "pgrep -u irc -f /var/lib/ratbox/${name}",
    }

    if $services == true {
        include irc::services

        if ! $servpass {
            fail("\$servpass must be defined.")
        }

        file { [ "/var/lib/ratbox-services/${name}",
                 "/var/lib/ratbox-services/${name}/etc",
                 "/var/lib/ratbox-services/${name}/etc/ratbox-services",
                 "/var/lib/ratbox-services/${name}/usr",
                 "/var/lib/ratbox-services/${name}/usr/share",
                 "/var/lib/ratbox-services/${name}/usr/share/ratbox-services",
                 "/var/lib/ratbox-services/${name}/usr/share/ratbox-services/help",
                 "/var/lib/ratbox-services/${name}/usr/share/ratbox-services/langs",
                 "/var/lib/ratbox-services/${name}/var",
                 "/var/lib/ratbox-services/${name}/var/log",
                 "/var/lib/ratbox-services/${name}/var/log/ratbox-services",
                 "/var/lib/ratbox-services/${name}/var/run",
                 "/var/lib/ratbox-services/${name}/var/run/ratbox-services", ]:
            ensure  => directory,
            owner   => "irc",
            group   => "irc",
            mode    => "0600",
            before  => Service["ratbox-services-${name}"],
            require => File["/var/lib/ratbox-services"],
        }

        File["/var/lib/ratbox-services/${name}/usr/share/ratbox-services/help"] {
            source  => "/usr/share/ratbox-services/help",
            recurse => true,
        }

        file { "/var/lib/ratbox-services/${name}/etc/ratbox-services/ratbox-services.conf":
            ensure  => present,
            mode    => "0600",
            owner   => "irc",
            group   => "irc",
            content => template("irc/ratbox-services.conf.erb"),
            before  => Service["ratbox-services-${name}"],
            notify  => Service["ratbox-services-${name}"],
            require => File["/var/lib/ratbox-services/${name}/etc/ratbox-services"],
        }

        file { "/var/lib/ratbox-services/${name}/etc/ratbox-services/ratbox-services.db":
            ensure  => present,
            mode    => "0600",
            owner   => "irc",
            group   => "irc",
            source  => "/etc/ratbox-services/ratbox-services.db",
            replace => false,
            before  => Service["ratbox-services-${name}"],
            require => File["/var/lib/ratbox-services/${name}/etc/ratbox-services"],
        }

        file { "/etc/init.d/ratbox-services-${name}":
            ensure  => present,
            mode    => "0755",
            owner   => "root",
            group   => "root",
            content => template("irc/ratbox-services.init.erb"),
            before  => Service["ratbox-services-${name}"],
            notify  => Exec["enable-ratbox-services-${name}"],
        }

        exec { "enable-ratbox-services-${name}":
            path        => "/bin:/usr/bin:/sbin:/usr/sbin",
            command     => "update-rc.d ratbox-services-${name} defaults",
            refreshonly => true,
            before      => Service["ratbox-services-${name}"],
        }

        service { "ratbox-services-${name}":
            ensure  => running,
            enable  => true,
            status  => "pgrep -u irc -f /var/lib/ratbox-services/${name}",
        }
    }

}
